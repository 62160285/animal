/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public abstract class Reptile extends Animal {
    
    public Reptile(String name, int numberOfLegs) {
        super(name, numberOfLegs);
    }
   public abstract void crawl(); 
}
