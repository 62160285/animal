/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Gun");
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is Landanimal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("h1 is animal ? " + (a1 instanceof Animal));
        System.out.println("h1 is Landanimal ? " + (a1 instanceof Reptile));

        Cat c1 = new Cat("Rosemary");
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        c1.run();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is Landanimal ? " + (c1 instanceof LandAnimal));

        Dog d1 = new Dog("Voxx");
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is Landanimal ? " + (d1 instanceof LandAnimal));

        Fish f1 = new Fish("Jonus");
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        f1.swim();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is Landanimal ? " + (f1 instanceof AquaticAnimal));

        Crab c2 = new Crab("Caramel");
        c2.eat();
        c2.walk();
        c2.speak();
        c2.sleep();
        c2.swim();
        System.out.println("c2 is animal ? " + (c2 instanceof Animal));
        System.out.println("c2 is Landanimal ? " + (c2 instanceof AquaticAnimal));

        Bat b1 = new Bat("Cigar");
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        b1.fly();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Landanimal ? " + (b1 instanceof Poultry));

        Bird b2 = new Bird("Jodan");
        b2.eat();
        b2.walk();
        b2.speak();
        b2.sleep();
        b2.fly();
        System.out.println("b2 is animal ? " + (b2 instanceof Animal));
        System.out.println("b2 is Landanimal ? " + (b2 instanceof Poultry));
        
        Crocodile c3 = new Crocodile("Jay");
        c3.eat();
        c3.walk();
        c3.speak();
        c3.sleep();
        c3.crawl();
        System.out.println("c3 is animal ? " + (c3 instanceof Animal));
        System.out.println("c3 is Landanimal ? " + (c3 instanceof Reptile));
        
        Snake s1 = new Snake("Miw");
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        s1.crawl();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is Landanimal ? " + (s1 instanceof Reptile));
    }
}
